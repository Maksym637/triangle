def is_triangle(a, b, c):
    if (a == 0 or b == 0 or c == 0):
        return False

    return (
        ((a + b) > c) and
        ((b + c) > a) and 
        ((a + c) > b)
    )
